package TareaFormulario;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

public class Corredor extends Thread {
	public JProgressBar progresBar;
	public JTextArea mensajes;
	JLabel nombre;
	static int FINAL = 100;
	
	public Corredor(String str, JProgressBar jpb, JTextArea jta, JLabel jl) {
		super(str);
		this.progresBar = jpb;
		this.mensajes = jta;
		this.nombre = jl;
		this.progresBar.setMinimum(0);
		this.progresBar.setMaximum(FINAL);		
		this.nombre.setText(str);
	}
	
	public void run(){
		for (int i = 1; i <= FINAL; i++) {
			System.out.println("El corredor " + getName() + " se encuentra ahora en: "+ i + "\n");
			progresBar.setValue(i);
			mensajes.append("El corredor " + getName() + " se encuentra ahora en: "+ i + "\n");
			//creamos el n�mero al azar
			int valorDado = (int) (Math.random()*10+1);
			
			//si el n�mero al azar es menor a 5, entonces descanzar� 3 segundos o menos seg�n sea
			//el random para descansar
			if(valorDado <= 5) {
				try {
					int dormir = (int) (Math.random() * 3000);
					sleep(dormir);
					mensajes.append("---El corredor " + getName() + " par� a descansar por: "+ dormir/1000.0+" segundos---\n");
					System.out.println("---El corredor " + getName() + " par� a descansarpor: "+ dormir/1000.0+ " segundos---\n");
				} catch (InterruptedException e) {
					System.out.println(e);
					mensajes.append(String.valueOf(e)+"\n");
				}
			}
			
		}
		System.out.println("******************************************** \n");
		System.out.println("Fin de la carrera para: " + this.getName() );
		System.out.println("******************************************** \n");

		mensajes.append("******************************************** \n");
		mensajes.append("Fin de la carrera para: " + this.getName() + "\n");
		mensajes.append("******************************************** \n");
		progresBar.setValue(0);
		

	}
	
	public void stop3() {
		if(this.isAlive()) {
			try {
				sleep((int) (3000));
				mensajes.append("******************************************** \n");
				mensajes.append("El corredor " + this.getName() + " descanso por 3s \n");
				mensajes.append("*********y regresa a la carrera\"********* \n");
				mensajes.append("******************************************** \n");

				
				System.out.println("******************************************** \n");				
				System.out.println("El corrdor " + this.getName() + " descanso por 3s \n");
				System.out.println("******************************************** \n");
				
			} catch (InterruptedException e) {
				System.out.println(e);
				mensajes.append(String.valueOf(e)+"\n");
			}
		}else {
			mensajes.append("*******El Hilo no esta vivo********* \n");
			System.out.println("*******El Hilo no esta vivo********* \n");
		}
		
	}
	
}
