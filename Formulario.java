package TareaFormulario;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;


import java.awt.Component;


import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Font;



public class Formulario {

	private JFrame frame;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Formulario window = new Formulario();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Formulario() {
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setResizable(false);
		frame.setBounds(100, 100, 799, 549);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		final JProgressBar progressBar_c1 = new JProgressBar();
		progressBar_c1.setForeground(Color.WHITE);
		progressBar_c1.setBackground(SystemColor.activeCaption);
		final JProgressBar progressBar_c2 = new JProgressBar();		
		progressBar_c2.setBackground(SystemColor.activeCaption);
		progressBar_c2.setForeground(Color.WHITE);
		final JProgressBar progressBar_c3 = new JProgressBar();
		progressBar_c3.setForeground(Color.WHITE);
		progressBar_c3.setBackground(SystemColor.activeCaption);
		final JLabel lblNewLabel_c1 = new JLabel("Corredor 1");		
		final JLabel lblNewLabel_c2 = new JLabel("Corredor 2");		
		final JLabel lblNewLabel_c3 = new JLabel("Corredor 3");
		
		final JTextArea textArea = new JTextArea();
		textArea.setForeground(Color.GRAY);
		textArea.setBackground(Color.WHITE);
		
		Corredor [] corredores = {
				new Corredor("Jean", progressBar_c1, textArea, lblNewLabel_c1),
				new Corredor("Juan", progressBar_c2, textArea, lblNewLabel_c2),
				new Corredor("Pablo", progressBar_c3, textArea,lblNewLabel_c3)
		};
	        
		JButton btnEmpezarTodos = new JButton("Empezar carrera");
		btnEmpezarTodos.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		JButton detenerCarrera = new JButton("Detener Carrera");
		detenerCarrera.setEnabled(false);
		detenerCarrera.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
			
		final JScrollPane scrollPane = new JScrollPane();
		
		JLabel lblNewLabel = new JLabel("por:Jean Paul Mamani motta");
		
		
		JLabel lblHttpssitesgooglecomsitejavaejercicios = new JLabel("https://sites.google.com/site/javaejercicios/");
		
		//***************************CorredorA		
		
		JButton btnIniciarA = new JButton("Iniciar");
		btnIniciarA.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		JButton btnPararA = new JButton("Parar 3s");
		btnPararA.setEnabled(false);
		btnPararA.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		JButton btnDetenerA = new JButton("Detener");
		btnDetenerA.setEnabled(false);
		btnDetenerA.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		
		
		btnIniciarA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Si es que el hilo esta vivo entonces s�lo lo continuamos
				if(corredores[0].isAlive()) {
					corredores[0].resume();
					btnIniciarA.setEnabled(false);
					btnPararA.setEnabled(true);
					btnDetenerA.setEnabled(true);
				}else {
				//Si esta muerto, lo inicamos
					corredores[0].start();
					btnIniciarA.setEnabled(false);
					btnPararA.setEnabled(true);
					btnDetenerA.setEnabled(true);
				}
											
				
			}
		});

		
		
		btnPararA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				corredores[0].stop3();
			}
		});
		
		
		
		btnDetenerA.addActionListener(new ActionListener() {
			int estado = 0;
			public void actionPerformed(ActionEvent e) {
				
				switch(estado) {
					case 0:
						estado = 1;
						btnDetenerA.setText("Continuar");
						btnPararA.setEnabled(false);
						
						corredores[0].suspend();
						corredores[0].mensajes.append("*******El corredor " + corredores[0].getName() + " se detuvo FORZOSAMENTE********* \n");
						System.out.println("*******El corredor " + corredores[0].getName() + " se detuvo FORZOSAMENTE********* \n");
																	
						
						break;
						
					case 1:
						
						corredores[0].resume();
						btnDetenerA.setText("Detener");
						estado = 0;
						btnPararA.setEnabled(true);
						break;					
				}
			}
		});
		//************************************
		//***************************CorredorB
		
		JButton btnIniciarB = new JButton("Iniciar");
		btnIniciarB.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		JButton btnPararB = new JButton("Parar 3s");
		btnPararB.setEnabled(false);
		btnPararB.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		JButton btnDetenerB = new JButton("Detener");
		btnDetenerB.setEnabled(false);
		btnDetenerB.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		
		btnIniciarB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
				if(corredores[1].isAlive()) {
					corredores[1].resume();
					btnIniciarB.setEnabled(false);
					btnPararB.setEnabled(true);
					btnDetenerB.setEnabled(true);
				}else {
					corredores[1].start();
					btnIniciarB.setEnabled(false);
					btnPararB.setEnabled(true);
					btnDetenerB.setEnabled(true);
				}	
			}
		});
		
		
		
		btnPararB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				corredores[1].stop3();
			}
		});
		
		
		
	
		
		btnDetenerB.addActionListener(new ActionListener() {
			int estado = 0;
			public void actionPerformed(ActionEvent e) {
				
				switch(estado) {
				case 0:
					estado = 1;
					btnDetenerB.setText("Continuar");
					btnPararB.setEnabled(false);
					
					corredores[1].suspend();
					corredores[1].mensajes.append("*******El corredor " + corredores[1].getName() + " se detuvo FORZOSAMENTE********* \n");
					System.out.println("*******El corredor " + corredores[1].getName() + " se detuvo FORZOSAMENTE********* \n");
																
					
					break;
					
				case 1:
					
					corredores[1].resume();
					btnDetenerB.setText("Detener");
					estado = 0;
					btnPararB.setEnabled(true);
					break;					
			}
				
				
			}
		});
		//*************************************
		
		//***************************CorredorC
		JButton btnIniciarC = new JButton("Iniciar");		
		btnIniciarC.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		JButton btnPararC = new JButton("Parar 3s");
		btnPararC.setEnabled(false);
		btnPararC.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		JButton btnDetenerC = new JButton("Detener");
		btnDetenerC.setEnabled(false);
		btnDetenerC.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 11));
		
		
		btnIniciarC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				

				if(corredores[2].isAlive()) {
					corredores[2].resume();
					btnIniciarC.setEnabled(false);
					btnPararC.setEnabled(true);
					btnDetenerC.setEnabled(true);
				}else {
					corredores[2].start();
					btnIniciarC.setEnabled(false);
					btnPararC.setEnabled(true);
					btnDetenerC.setEnabled(true);
				}
			}
		});
		
		

		btnPararC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				corredores[2].stop3();
			}
		});
		
		
		
		
		btnDetenerC.addActionListener(new ActionListener() {
			int estado = 0;
			public void actionPerformed(ActionEvent e) {
				
				switch(estado) {
				case 0:
					estado = 1;
					btnDetenerC.setText("Continuar");
					btnPararC.setEnabled(false);
					
					corredores[2].suspend();
					corredores[2].mensajes.append("*******El corredor " + corredores[2].getName() + " se detuvo FORZOSAMENTE********* \n");
					System.out.println("*******El corredor " + corredores[2].getName() + " se detuvo FORZOSAMENTE********* \n");
																
					
					break;
					
				case 1:
					
					corredores[2].resume();
					btnDetenerC.setText("Detener");
					estado = 0;
					btnPararC.setEnabled(true);
					break;					
			}
				
				
			}
				
		});
		//******************************************
		
		//******************************************
		//Empezar Carrera para todos
		btnEmpezarTodos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText(null);
				for(int i = 0; i < corredores.length; i++) {
					if(!corredores[i].isAlive()) {
						corredores[i].start();
						switch(i){
						case 0:
							btnIniciarA.setEnabled(false);
							btnPararA.setEnabled(true);
							btnDetenerA.setEnabled(true);	
							break;
							
						case 1:
							btnIniciarB.setEnabled(false);
							btnPararB.setEnabled(true);
							btnDetenerB.setEnabled(true);	
							break;
							
						case 2:
							btnIniciarC.setEnabled(false);
							btnPararC.setEnabled(true);
							btnDetenerC.setEnabled(true);	
							break;
							
						}
						
					}
										
				}
				btnEmpezarTodos.setEnabled(false);
				detenerCarrera.setEnabled(true);
				
				
			}
		});
		//******************************************
		
		
		//Detener Carrera para todos		
		detenerCarrera.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(corredores[0].isAlive() && corredores[1].isAlive() && corredores[2].isAlive()) {
					
				}
				for(int i = 0; i < corredores.length; i++) {
					//si el hilo esta vivo, entonces lo paramos.
					if(corredores[i].isAlive()) {
						corredores[i].suspend();				
						
						
						switch(i){
						case 0:
							btnIniciarA.setEnabled(true);
							btnPararA.setEnabled(false);
							btnDetenerA.setEnabled(false);	
							break;
							
						case 1:
							btnIniciarB.setEnabled(true);
							btnPararB.setEnabled(false);
							btnDetenerB.setEnabled(false);	
							break;
							
						case 2:
							btnIniciarC.setEnabled(true);
							btnPararC.setEnabled(false);
							btnDetenerC.setEnabled(false);	
							break;
							
						}
						
					}
				}
				textArea.append("*****************************");
				textArea.append("LA CARRERA SE DETUVO PARA TODOS");
				textArea.append("*****************************");
				
				System.out.println("*****************************");
				System.out.println("LA CARRERA SE DETUVO PARA TODOS");
				System.out.println("*****************************");
			}
		});
		//******************************************
		
		
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblNewLabel)
							.addContainerGap(647, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblHttpssitesgooglecomsitejavaejercicios)
							.addContainerGap(571, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(10)
									.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addGroup(groupLayout.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
											.addComponent(detenerCarrera, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGroup(groupLayout.createSequentialGroup()
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(btnEmpezarTodos))))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel_c1)
										.addComponent(lblNewLabel_c3)
										.addComponent(lblNewLabel_c2))
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(progressBar_c1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
										.addComponent(progressBar_c3, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE)
										.addComponent(progressBar_c2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addGap(35)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
												.addGroup(groupLayout.createSequentialGroup()
													.addComponent(btnPararB)
													.addGap(18)
													.addComponent(btnIniciarB))
												.addGroup(groupLayout.createSequentialGroup()
													.addComponent(btnPararA)
													.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
													.addComponent(btnIniciarA)))
											.addGap(18)
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(btnDetenerA)
												.addComponent(btnDetenerB)))
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(btnPararC)
											.addGap(18)
											.addComponent(btnIniciarC)
											.addGap(18)
											.addComponent(btnDetenerC)))))
							.addGap(74))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(13)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel_c1)
								.addComponent(progressBar_c1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(btnPararA)
									.addComponent(btnDetenerA)
									.addComponent(btnIniciarA)))
							.addGap(28)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
									.addComponent(progressBar_c2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(lblNewLabel_c2))
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
									.addComponent(btnPararB)
									.addComponent(btnIniciarB)
									.addComponent(btnDetenerB)))
							.addGap(14)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnPararC, Alignment.TRAILING)
								.addComponent(progressBar_c3, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_c3, Alignment.TRAILING)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(105)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnIniciarC)
								.addComponent(btnDetenerC))))
					.addGap(22)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 361, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(86)
							.addComponent(btnEmpezarTodos)
							.addPreferredGap(ComponentPlacement.RELATED, 127, Short.MAX_VALUE)
							.addComponent(detenerCarrera)
							.addGap(102)))
					.addGap(37)
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(lblHttpssitesgooglecomsitejavaejercicios)
					.addContainerGap())
		);
		groupLayout.linkSize(SwingConstants.VERTICAL, new Component[] {progressBar_c1, progressBar_c2, progressBar_c3});
		
		
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);
		frame.getContentPane().setLayout(groupLayout);
		
		
		
			
		
	}	
}
